Cosmic veri tabanında bulunan CosmicMutantExportCensus.tsv adlı dosya her satırı bir mutasyonu barındır. Cosmic buna benzer pek çok farklı dosyayı içeren, kanserin oluşmasında etkili nesilden nesile iletilmeyen(somatik) varyasyonların veri tabanıdır. Burada dosyalar genelde tsv(tab separated file) formatı ile saklanmaktadır. Bu yaklaşık 1 milyon 600 satırdan ve 41 sütundan oluşmaktadır. Dosya mutasyonlu genin adı, hasta id, genin bulunduğu organ, kanserin cinsi gibi pek çok bilgi içermektedir. Diğer satırlara ait bilgiler şu şekildedir.



[1:A] Gene name -Verilerin COSMIC'de bulunan gen adı. Çoğu durumda bu kabul edilen HGNC kimliği*

*[2:B] Accession Number - Genin transkript tanımlayıcısı.*

*[3:C] Gene CDS length - Gen uzunluğu (baz çifti) sayılır.*

*[4:D] HGNC id -Eğer gen HGNC içindeyse bu id, HGNC'ye bağlanmasına yardımcı olur.*

*[5:E] Sample name,Sample id,Id tumour - Bir örnek, mutasyon açısından incelenen bir tümörün bir kısmının bir örneğidir. Örnek adı bir dizi kaynaktan elde edilebilir. Çoğu durumda hücre satırı adından kaynaklanır. Diğer kaynaklar arasında ek açıklamalar tarafından atanan adlar veya anonimleştirme işlemi sırasında atanan artan bir sayı bulunur. Tek bir tümörden birkaç örnek alınabilir ve bir kişiden birkaç tümör elde edilebilir. COSMIC veritabanındaki bir örneği tanımlamak için bir örnek kimliği kullanılır. Aynı örnek veritabanına farklı kağıtlardan birden çok kez girilmiş ise birden fazla kimlik olabilir.*

*[8:H] Primary Site - Numunenin kaynaklandığı birincil doku / kanser. Doku sınıflandırması hakkında daha fazla detay buradan temin edilebilir. COSMIC'te, farklı kağıtlar arasında çok değişkenlik gösterdiğinden, doku tipleri ve alt tipleri için standart sınıflandırma sistemimiz vardır.*

*[9:I] Site Subtype 1 - Orjinal numunelerinin alt sınıflaması (seviye 1).*

*[10:J] Site Subtype 2 - Orijin numunelerinin alt sınıflaması (seviye 2).*

*[11:K] Site Subtype 3 - Orijin numunelerinin alt sınıflaması (seviye 3).*

*[12:L] Primary Histology - Numunenin histolojik sınıflandırması.*

*[13:M] Histology Subtype 1 - Numunenin diğer histolojik sınıflandırması (seviye 1).*

*[14:N] Histology Subtype 2 - Numunenin diğer histolojik sınıflandırması (seviye 2).*

*[15:O] Histology Subtype 3 - Numunenin diğer histolojik sınıflandırması (seviye 3).*

*[16:P] Genome-wide screen - tüm genom / ekzom dizilirse.*

*[17:Q] GENOMIC_MUTATION_ID - Genomik mutasyon tanımlayıcısı (COSV), varyantın genom üzerindeki kesin pozisyonunu gösterir. Bu tanımlayıcı, sürümün farklı sürümleri arasında izlenebilir ve kararlıdır.*

*[18:R] LEGACY_MUTATION_ID - Mevcut COSM mutasyon tanımlayıcılarını temsil edecek eski mutasyon tanımlayıcı (COSM)*

*[19:S] MUTATION_ID - Mutasyonu benzersiz bir şekilde temsil eden bir dahili mutasyon tanımlayıcı.*

*[20:T] Mutation CDS - Nükleotit sekansında meydana gelen değişiklik. Biçimlendirme, peptit dizisi için kullanılan yöntemle aynıdır.*

*[21:U] Mutation AA - Peptit sekansında meydana gelen değişiklik. Biçimlendirme, İnsan Genomu Varyasyon Derneği tarafından yapılan önerilere dayanmaktadır. Her türün açıklaması, Mutasyona Genel Bakış sayfası bağlantısı izlenerek bulunabilir.*

*[22:V] Mutation Description - Mutasyon tipi (ikame, silme, yerleştirme, kompleks, füzyon vb.)*

*[23:W] Mutation zygosity - Mutasyonun örnek içinde homozigot, heterozigot veya bilinmediği.*

*[24:X] LOH - LOH Gende örnekteki heterozigotluk kaybının rapor edilip edilmediğine dair bilgi: evet, hayır veya bilinmiyor.*

*[25:Y] GRCh -Kullanılan koordinat sistemi*  

*[26:Z] Mutation genome position - Mutasyonun genomik koordinatları.*

*[27:AA] Mutation strand - pozitif veya negatif*

*[28:AB] SNP - Bilinen tüm SNP'ler, 1000 genom projesi, dbSNP ve Sanger CGP sekanslamasından 378 normal (kanser olmayan) örnek panel tarafından tanımlanan 'y' olarak işaretlenir.*

*[29:AC] Resistance Mutation - mutasyon ilaç direnci sağlar (gen / ilaç ayrıntıları için CosmicResistanceMutations.tsv.gz dosyasına bakınız).*

*[30:AD] FATHMM prediction - FATHMM (Gizli Markov Modelleri ile Fonksiyonel Analiz) hakkında daha fazla bilgiyi* [*buradan*](http://fathmm.biocompute.org.uk) *edinebilirsiniz*[*![img](https://lh5.googleusercontent.com/mYngTuPH-caDTUET4TI5htKMIWJRqyILfhPnRKimQSbkg8SFCarZrXRGgY4RMh4Xe4o2bVSaIXQI-2WRtIyDoMi7nrCUUjEVH3vqyxhHwQJhknHoGZfpjaNAOEJIywtWHm3ItPwd)*](http://fathmm.biocompute.org.uk)

*[31:AE] FATHMM score - FATHMM-MKL fonksiyonel skoru 0 ila 1 arasında değişen bir p-değeridir. 0.5'in üzerindeki puanlar zararlıdır, ancak COSMIC'deki en önemli verileri vurgulamak için sadece> = 0.7 skorları 'Patojenik' olarak sınıflandırılır. Skor <= 0,5 ise mutasyonlar 'Nötr' olarak sınıflandırılır.*

*[32:AF] Mutation somatic status - Numunenin Onaylanmış Somatik, Önceden Raporlanmış veya bilinmeyen bir Varyant olduğu bildirildiğine dair bilgiler* 

*[33:AG] Pubmed_PMID - Yayının daha fazla ayrıntısını sunmak için yayınlanmış ile bağlantılı olarak örneğin not edildiği makalenin PUBMED ID'si.*

*[34:AH] Id Study - Bu örneği içeren çalışmaların benzersiz kimliklerini listeler.*

*[35:AI] Sample Type,Tumour origin - Numunenin tümör tipinin dahil edilmesinden kaynaklandığını açıklar.*

*[37:AK] Age - Numunenin yaşı (eğer bu bilgiler yayınlarla birlikte verilmişse).*

*[38:AL] Tier - 1 ya da 2.*

*[39:AM] HGVSP - Human Genome Variation Society peptide syntax.*

*[40:AN] HGVSC - Human Genome Variation Society coding dna sequence syntax (CDS).*

*[41:AO] HGVSG - Human Genome Variation Society genomic syntax (3' shifted).*



Bu tsv dosyası okunarak Liver-carcinoma hastaları bulundu ve idleri “*soru_1.csv*” dosyasına yazıldı. Bu işlem Python Pandas kütüphanesinden yardım alarak gerçekleştirilmiştir. Dosya boyutu büyük olduğu için pandas çok daha iyi sonuç vermiştir. Bu aşamada tsv dosyasının gereksiz kolonları silinerek daha küçük veri ile çalışmak mümkündür. Ardından bu hastaların mutasyonlu genlerinin bir listesini çıkarttık. Liver-carcinoma ve skin_carcinoma için bu hastalarda rastlanan genlere ve bu genlerin bulunma olasılıklarına baktık. Karaciğer için “*soru_2.csv*”, Deri hücresi için *“soru_3.csv”* dosyasına yazma işlemi yaptık. Ardından da hem skin_carcinoma hem liver_carcinoma da rastlanan genleri sıraladık. *“soru_4.csv*” dosyasında bu genleri yazdırma işlemi yaptık. Farklı sonuçlar çıkarmak için bu genleri ve değerlerini inceledik. Farklı sitelerden ve veri tabanlarından genlere ait özelliklere göz attık. 



Pandas ve Numpy kütüphaneleri veri analizi için kullanılan açık kaynaklı kütüphanelerdir. Pandas bu işlemleri daha optimize bir şekilde gerçekleştirdiği için hız konusunda avantaj sağlamaktadır. Yine de biz başlangıçta veriyi parçalara bölerek boyutu daha küçük bir dosya üzerinde çalışmayı uygun bulduk. Pandas’ ın sağladığı Dataframe ve series yapıları ve join, merge gibi işlemlerle çok daha hızlı okuma ve yazma yapabilmekteyiz. Bunlara ek olarak veriler görselleştirme yoluna gidilebilir. Böylece daha anlaşılır sonuçlar ortaya konulabilir.