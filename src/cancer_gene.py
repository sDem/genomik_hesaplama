import pandas as pd

#tsv dosyası pandas ile okunur
mutations = pd.read_table('CosmicMutantExportCensus.tsv')

liver_carcinoma = mutations[(mutations['Primary site'] == "liver") & (mutations['Primary histology'] == "carcinoma")]

total_gene_length = len(liver_carcinoma.index) + 1

#gene name kolonu sıralanır ve tekrarları ile kayıt edilir.
liver_carcinoma_genes = liver_carcinoma["Gene name"].value_counts(dropna=False)
data = pd.DataFrame((liver_carcinoma_genes/total_gene_length))

#iki data birleştirilir.
gene_possibility = pd.concat([liver_carcinoma_genes,data],axis=1)

gene_possibility.to_csv("../Results/soru_2.csv",sep='\t',index=True,header=False)

