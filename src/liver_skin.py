import pandas as pd

# CosmicMutantExportCensus dosyası okunur
mutations = pd.read_table('CosmicMutantExportCensus.tsv')

# Gereksiz kolonlar silinir
rows_to_drop = ['Accession Number', 'Gene CDS length', 'HGNC ID',
       'Sample name', 'ID_sample', 'ID_tumour','Site subtype 1', 'Site subtype 2', 'Site subtype 3', 
       'Histology subtype 1', 'Histology subtype 2','Histology subtype 3', 'Genome-wide screen', 'GENOMIC_MUTATION_ID',
       'LEGACY_MUTATION_ID', 'MUTATION_ID', 'Mutation CDS', 'Mutation AA',
       'Mutation Description', 'Mutation zygosity', 'LOH', 'GRCh',
       'Mutation genome position', 'Mutation strand', 'SNP',
       'Resistance Mutation', 'FATHMM prediction', 'FATHMM score',
       'Mutation somatic status', 'Pubmed_PMID', 'ID_STUDY', 'Sample Type',
       'Tumour origin', 'Age', 'Tier', 'HGVSP', 'HGVSC', 'HGVSG']
mutations = mutations.drop(rows_to_drop, axis=1)

#iki farklı dataframe oluşturularak mergelenir ve ilk satır csv dosyasına yazdırılır. Gerekirse diğer bilgiler de eklenebilir.
liver_carcinoma = mutations[(mutations["Primary site"] == "liver") & (mutations["Primary histology"] == "carcinoma")]
skin_carcinoma = mutations[(mutations["Primary site"] == "skin") & (mutations["Primary histology"] == "carcinoma")]

common_values = pd.merge(liver_carcinoma,skin_carcinoma, how='inner', left_on='Gene name', right_on='Gene name')
data = common_values["Gene name"].value_counts(dropna=False)

#ortak genleri yazdırıyoruz soru_4.csv dosyasına
data = pd.DataFrame(data.index)
data.to_csv("../Results/soru_4.csv",sep='\t',index=False,header=False)