import csv
import pandas as pd

#Pandas Kütüphanesi ile tsv dosyası okunur.
mutations = pd.read_table('CosmicMutantExportCensus.tsv')

#karaciğer ve carcinoma hastalarının idleri ve genleri sorgulanır
liver_carcinoma = mutations[(mutations['Primary site'] == "liver") & (mutations['Primary histology'] == "carcinoma")][["Accession Number","Gene name"]]

liver_carcinoma.to_csv("../Results/soru1.csv",sep='\t',index=False,header=False)